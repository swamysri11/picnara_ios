//
//  homeViewController.swift
//  Picnara
//
//  Created by RAUNAK SINHA on 09/04/19.
//  Copyright © 2019 RAUNAK SINHA. All rights reserved.
//

import UIKit

class homeViewController: UIViewController {
    
    @IBOutlet weak var sideMenu: UIView!
    var sideMenuOpen = false

    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    @IBAction func toggleSideMenu(_ sender: Any) {
        self.openAndCloseMenu()
        print("side menu")
    }
    @IBAction func notif(_ sender: Any) {
        performSegue(withIdentifier: "notifSegue", sender: nil)
        }
    func openAndCloseMenu() {
        if sideMenuOpen {
            sideMenuOpen = false
            view.addConstraint(NSLayoutConstraint(item: sideMenu, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: -200))
        }else {
            sideMenuOpen = true
            view.addConstraint(NSLayoutConstraint(item: sideMenu, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0))
        }
    }

    
    override func viewDidLoad() {
         super.viewDidLoad()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
