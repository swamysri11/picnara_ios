//
//  ViewController.swift
//  Picnara
//
//  Created by RAUNAK SINHA on 08/04/19.
//  Copyright © 2019 RAUNAK SINHA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    
    @IBAction func signup(_ sender: Any) {
        performSegue(withIdentifier: "signupSegue", sender: nil)
    }
    
    
    @IBAction func submit(_ sender: Any) {
        
        if email.text == "" || password.text == "" {
            displayAlert(title: "Wrong Information", message: "You must provide both email and password")
        }else if email.text == "admin@gmai.com" || password.text == "admin" {
            performSegue(withIdentifier: "submitSegue", sender: nil)
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func displayAlert(title : String,message : String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }

}

