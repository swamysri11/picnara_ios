//
//  TableViewCell.swift
//  Picnara
//
//  Created by RAUNAK SINHA on 11/04/19.
//  Copyright © 2019 RAUNAK SINHA. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {


    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var about: UILabel!
    @IBOutlet weak var budget: UILabel!
    @IBOutlet weak var praposal: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var postedOn: UILabel!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var number: UIImageView!
    @IBOutlet weak var mail: UIImageView!
}
